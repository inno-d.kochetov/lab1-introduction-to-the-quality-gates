FROM openjdk:14-jdk-alpine
COPY target/main-0.0.1-SNAPSHOT.jar /usr/local/root.jar
CMD java -jar -Dserver.port="$PORT" /usr/local/root.jar